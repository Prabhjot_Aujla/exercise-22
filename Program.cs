﻿using System.Collections.Generic;
using System;


namespace exercise_22
{
    class Program
    {
        public static void Main(string[] args)
        {
            //Create an Array
            var movies = new string[5] {"My name is Jeff", "Drake", "21 Jump Street","Batman", "Superman"};

            //Sort the Array in Alhpabetical Order
            Array.Sort(movies);

            //Print the Array to the screen
            Console.WriteLine(String.Join(",", movies));
            
           var foodplaces = new List<string> {"LoneStar", "MumbaiMasala", "CoffeeClub", "ChineseTakeaways", "CheesecakeShop"};
           foodplaces.Sort();
           
           foodplaces.Remove("CoffeeClub");

           foodplaces.Sort();

           

           Console.WriteLine(String.Join(",", foodplaces));

        }
    }
}
